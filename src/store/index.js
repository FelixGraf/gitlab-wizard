import Vue from 'vue';
import Vuex from 'vuex';
import VuexPersistence from 'vuex-persist';

import mergeRequests from './modules/mergeRequests.js';

Vue.use(Vuex);

const vuexLocal = new VuexPersistence({
    storage: window.localStorage,
    modules: ['mergeRequests'],
});

export default new Vuex.Store({
    state: {
        appVersion: process.env.VUE_APP_VERSION,
    },
    modules: {
        mergeRequests,
    },
    plugins: [vuexLocal.plugin],
});
