import sleep from '../../utils/sleep.js';
import { setValue } from '../../utils/inputUtils.js';

const templateDropdownMenu = document.querySelector('button[data-field-name="issuable_template"]')?.parentNode;

const openTemplateDropdown = () => document.querySelector('button[data-field-name="issuable_template"]').click();

const confirmApplyTemplate = () => document.querySelector('.js-override-template').click();

const searchAndSelectTemplate = async template => {
    const searchInput = templateDropdownMenu.querySelector('input[type="search"]');
    searchInput.click();
    setValue(searchInput, template);

    await sleep(500);

    const foundTemplates = templateDropdownMenu.querySelectorAll('.dropdown-content li a');

    for (const foundTemplate of foundTemplates) {
        if (foundTemplate.textContent.trim().toLowerCase() === template.toLowerCase()) {
            foundTemplate.click();
            break;
        }
    }
};

const fillTemplate = async ({ template }) => {
    if (!template || !templateDropdownMenu) {
        return;
    }

    openTemplateDropdown();

    await sleep(500);

    await searchAndSelectTemplate(template);

    await sleep(200);

    confirmApplyTemplate();
};

export default fillTemplate;
