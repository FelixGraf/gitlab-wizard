import sleep from '../../utils/sleep.js';
import { collectDropdownValues, setValue } from '../../utils/inputUtils.js';
import { getUserNameFromElement } from '../../utils/utils';

const openApprovalGroupModal = () => document.querySelector('button[data-qa-selector="add_approvers_button"]').click();

const submitApprovalGroupModal = () =>
    document.querySelector('#mr-edit-approvals-create-modal___BV_modal_footer_ .btn-confirm').click();

const searchAndSelectApprover = async approver => {
    const searchInput = document
        .querySelector('[data-qa-selector="member_select_field"]')
        .parentElement.querySelector('input.select2-input');
    searchInput.click();
    setValue(searchInput, approver);

    const suggestions = await collectDropdownValues(document, 'ul.select2-results > li.select2-result-selectable');

    for (const suggestion of suggestions) {
        const suggestionUserElement = suggestion.querySelector('.user-username');
        if (getUserNameFromElement(suggestionUserElement) === `@${approver.toLowerCase()}`) {
            suggestion.dispatchEvent(
                new MouseEvent('mouseup', {
                    bubbles: true,
                    cancelable: true,
                })
            );
            break;
        }
    }
};

const fillGroup = async ({ name, approvers }) => {
    // Fill group name
    const groupNameInput = document.querySelector('input[data-qa-selector="rule_name_field"]');
    setValue(groupNameInput, name);

    for (const approver of approvers) {
        await searchAndSelectApprover(approver);
    }
};

const fillNumberApprovalsRequired = numberApprovalsRequired => {
    if (numberApprovalsRequired || numberApprovalsRequired === 0) {
        const numberApprovalsRequiredInput = document.querySelector('td.js-approvals-required input');
        setValue(numberApprovalsRequiredInput, numberApprovalsRequired);
    }
};

const fillApprovers = async ({ group, numberApprovalsRequired }) => {
    if (group.approvers.length) {
        openApprovalGroupModal();
        await sleep(500);
        await fillGroup(group);
        submitApprovalGroupModal();
    }

    await sleep(200);

    fillNumberApprovalsRequired(numberApprovalsRequired);
};

export default fillApprovers;
