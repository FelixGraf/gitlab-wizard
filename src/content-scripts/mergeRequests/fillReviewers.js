import sleep from '../../utils/sleep.js';
import { collectDropdownValues, setValue } from '../../utils/inputUtils.js';
import { getUserNameFromElement } from '../../utils/utils';

// TODO Clean up the code as its very similar to the other fill commands

const reviewersDropdownMenu = document.querySelector('.dropdown-menu-reviewer');

const openReviewersDropdown = () =>
    document.querySelector('.merge-request-reviewer button.dropdown-menu-toggle').click();

const closeReviewersDropdown = () =>
    document.querySelector('.merge-request-reviewer button.dropdown-menu-close').click();

const searchAndSelectReviewer = async reviewer => {
    await sleep(100);

    const searchInput = reviewersDropdownMenu.querySelector('input[type="search"]');
    searchInput.click();
    setValue(searchInput, reviewer);

    await sleep(1000);

    const suggestions = await collectDropdownValues(
        reviewersDropdownMenu,
        '.dropdown-content .dropdown-menu-user-link'
    );

    for (const suggestion of suggestions) {
        const suggestionUserElement = suggestion.querySelector('.dropdown-menu-user-username');
        if (getUserNameFromElement(suggestionUserElement) === `@${reviewer.toLowerCase()}`) {
            suggestion.click();
            await sleep(100);
            break;
        }
    }
};

const fillReviewers = async ({ reviewers }) => {
    if (!reviewers.length) {
        return;
    }
    openReviewersDropdown();

    await sleep(500);

    for (const reviewer of reviewers) {
        await searchAndSelectReviewer(reviewer);
    }

    closeReviewersDropdown();
};

export default fillReviewers;
